![flutter dart](media/flutterdart.png)![graphql](media/graphql.png)

![license](media/license-BSDClause2-blue.svg)
![framework](media/framework-flutter-lightgrey.svg)
![querylang](media/querylang-graphql-yellowgreen.svg)
# githuber

Githubber is a mobile application exploring github stuff that uses GrraphQL to query github's api. 


## Table of Contents

* [About the Project](#about-the-project)
  * [Built With](#built-with)
* [Getting Started](#getting-started)
  * [Prerequisites](#prerequisites)
  * [Installation](#installation)
* [Usage](#usage)
* [Roadmap](#roadmap)
* [Contributing](#contributing)
* [License](#license)
* [Contact](#contact)
* [Acknowledgements](#acknowledgements)

# about-the-project

Githuber is a mobile app to explore agithub users repos using gpaphql to query the github api.

## built-with

* Flutter and Dart
* flutter platform Widgets
* Modular for bloc and widget dependency injection
* flutter simple dependency injection for non widget dep injection

# getting-started

Getting started is somewhat easy

## prerequisites

If yu have not done so yet, you need to install the [flutter sdk](https://flutter.dev/docs/get-started/install) and of course the mpbile sdks and an IDE. The android side can eb downloaded alond with the [ide here](https://developer.android.com/studio). While XCode and ios sdk can be found at [Apple's Dev site](https://developer.apple.com/xcode/).

## installation

Just clone it, than read the templates in the tempalte foldeer to get an idea of where to put your github access token and than build and run the app.


# usage

Not taking contributions on this project but you can cloen it and modify it for your won playing and use. Read the templates to get on idea of how to setup the build variants gitignored with your own github personal token api key.

# roadmap

Just getting an implementation running

# contributing

Not at this time taking contributions fopr this project. But, you can clone it and modify it for your own use.

# license

BSD Clause 2 License


# contact

Look, I work remotely. That means my phone is reserved for those clients tha tpay for my retainer-and-phone-support package. And, of course you devs and designers know by now that individual programing questions and individual design questions are best asked onn both reddit and stackoverflow to get your answer in a fast manner.

I can be reached via my email addy of fred DOT grott AT gmail DOT com.  If you are a develoepr ro designer and want to exchange expertise you may want to sign up for an keybase.io accoount and follow me at my [keybase.io profile](https://keybase.io/fredgrott).

Design wise, I have sm accounts with [Behance](https://www.behance.net/gwsfredgrott) and [Dribbble](https://dribbble.com/FredGrott).Developer wise, I have sm accounts with [github](https://github.com/fredgrott) and [gitlab](https://gitlab.com/fred.grott). And, biz wise I have sm accounts at [LinkedIN](https://www.linkedin.com/in/fredgrottstartupfluttermobileappdesigner) and [Xing](https://www.xing.com/profile/Fred_Grott/cv). Social wise, I have [twiiter](https://twitter.com/fredgrott).

# acknowledgments

Thank you Gogle Chrome team for spearheadig the start of both flutter and dart projects.

