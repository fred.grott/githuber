



// not sure yet why I cannot defien this in the main dart file
// or even if I need thehemes.dart import in the main top of file

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';


Brightness brightness = Brightness.light;





final ThemeData fgMaterialTheme = ThemeData(
  primarySwatch: Colors.purple,
  fontFamily: 'WorkSans',

);

final ThemeData fgMaterialDarkTheme =  ThemeData(
  brightness: Brightness.dark,
  fontFamily: 'WorkSans',
  primarySwatch: Colors.teal,

);

final CupertinoThemeData fgCupertinoTheme = CupertinoThemeData(
  // work-around current CuperinoApp using defualt roboto fonts
  // and google_fonts pluginnot realy ready for beta
  // other part is defining fonts in pubspec
  // backout not ready yet
  // still cannot set fonts 1-30-2020 when using the
  // flutter_platform_widgets plugin but not usre if its that
  //plugin's falut
  //textTheme: const CupertinoTextThemeData(
  //textStyle: TextStyle(fontFamily: 'WorkSans')
  //),


  brightness: brightness, // if null will use the system theme
  primaryColor: CupertinoDynamicColor.withBrightness(
    color: Colors.purple,
    darkColor: Colors.cyan,
  ),

);

// no no instead do jus the individual textstyles needed

