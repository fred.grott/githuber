// BSD Clause 2 Copyrigth 2020 Fred Grott(Fredrick Allan Grott)
const String addStar = r'''
  mutation AddStar($starrableId: ID!) {
    action: addStar(input: {starrableId: $starrableId}) {
      starrable {
        viewerHasStarred
      }
    }
  }
''';