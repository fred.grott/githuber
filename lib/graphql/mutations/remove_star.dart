// BSD Clause 2 Copyrigth 2020 Fred Grott(Fredrick Allan Grott)
const String removeStar = r'''
  mutation RemoveStar($starrableId: ID!) {
    action: removeStar(input: {starrableId: $starrableId}) {
      starrable {
        viewerHasStarred
      }
    }
  }
''';