// BSD Clause 2 Copyrigth 2020 Fred Grott(Fredrick Allan Grott)
import 'dart:async';

import 'package:flutter/widgets.dart';
import 'package:githuber/modules/repos/models.dart';
import 'package:graphql/client.dart';
import 'package:githuber/graphql/mutations/mutations.dart' as mutations;
import 'package:githuber/graphql/queries/read_respositories.dart' as queries;

class GithubRepository {
  final GraphQLClient client;

  GithubRepository({@required this.client}) : assert(client != null);

  Future<QueryResult> getRepositories(int numOfRepositories) async {
    final WatchQueryOptions _options = WatchQueryOptions(
      documentNode: gql(queries.readRepositories),
      variables: <String, int>{
        'nRepositories': numOfRepositories,
      },
      pollInterval: 4,
      fetchResults: true,
    );

    // ignore: unnecessary_await_in_return
    return await client.query(_options);
  }

  Future<QueryResult> toggleRepoStar(Repo repo) async {
    final  document =
    repo.viewerHasStarred ? mutations.removeStar : mutations.addStar;

    final MutationOptions _options = MutationOptions(
      // ignore: deprecated_member_use
      document:  document,
      variables: <String, String>{
        'starrableId': repo.id,
      },
    );

    // ignore: unnecessary_await_in_return
    return await client.mutate(_options);
  }
}