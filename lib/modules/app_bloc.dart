import 'dart:ui';

import 'package:flutter_modular/flutter_modular.dart';

class AppBloc extends Disposable {

  static Brightness brightness = brightness == Brightness.light
  ? Brightness.dark
      : Brightness.light;
  @override
  void dispose() {}
}
