import 'package:catcher/core/catcher.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:flutter_platform_widgets/flutter_platform_widgets.dart';

import '../app_themes.dart';
import '../themes_colors.dart';
import '../themes_textstyles.dart';
import '../widget_themes.dart';

class AppWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // hmm do I ned anootated region?
    // seems to work but eventually should move to using
    // annotedregion as I have nested widgets and diff appbars
    // per platforms
    SystemChrome.setEnabledSystemUIOverlays(SystemUiOverlay.values) ;
    // this for full screen
    SystemChrome.setEnabledSystemUIOverlays([SystemUiOverlay.bottom]);
    SystemChrome.setSystemUIOverlayStyle(const SystemUiOverlayStyle(
        systemNavigationBarIconBrightness: Brightness.light
    ));

    // need to add appbar themes for both
    // materialapp and cupertinoapp
    // as of 1-30-2020 still have to set
    // fontFamily in material theme for fonts
    // on all platforms ie no set font for iios separate
    // from android
    final materialTheme = ThemeData(
      // as of 2-7-2020 I still need to set this for appbar color
      primaryColor: Colors.purple,

      colorScheme: myLightColorScheme,
      textTheme: myMaterialTextTheme,
      buttonTheme: myButtonTheme,
    );
    final materialDarkTheme = ThemeData(
      brightness: Brightness.dark,
      colorScheme: myDarkColorScheme,
      textTheme: myMaterialTextTheme,
    );
    final cupertinoTheme =
    CupertinoThemeData(
      // work-around current CuperinoApp using defualt roboto fonts
      // and google_fonts pluginnot realy ready for beta
      // other part is defining fonts in pubspec
      // backout not ready yet
      // still cannot set fonts 1-30-2020 when using the
      // flutter_platform_widgets plugin but not usre if its that
      //plugin's falut
      //textTheme: const CupertinoTextThemeData(
      //textStyle: TextStyle(fontFamily: 'WorkSans')
      //),
        brightness: brightness, // if null will use the system theme
        primaryColor: primaryPurple,
        barBackgroundColor: barBackgroundPrimary,
        textTheme: myCupertinoTextTheme
    );
    return Theme(
      data: brightness == Brightness.light ? materialTheme : materialDarkTheme,
      child: PlatformProvider(
        builder: (context)=>PlatformApp(
          debugShowCheckedModeBanner: false,
          navigatorKey: Catcher.navigatorKey,
          localizationsDelegates: <LocalizationsDelegate<dynamic>>[
            DefaultMaterialLocalizations.delegate,
            DefaultWidgetsLocalizations.delegate,
            DefaultCupertinoLocalizations.delegate,
          ],
          title: 'Githuber',
        android: (_) {
          return MaterialAppData(
            theme: materialTheme,
            darkTheme: materialDarkTheme,
            themeMode: brightness == Brightness.light
                ? ThemeMode.light
                : ThemeMode.dark,
          );

        },
          ios: (_) => CupertinoAppData(
            theme: cupertinoTheme,
           ),
          builder:(BuildContext context, Widget widget) {
            Catcher.addDefaultErrorWidget(
                showStacktrace: true,
                customTitle: "Custom error title",
                customDescription: "Custom error description");
            return widget;
          },
          home: PlatformScaffold(
            iosContentPadding: true,
            appBar: PlatformAppBar(
              title: const Text('Githuber', ),
              android: (_) => myMaterialAppBarData,
              ios: (_) => myCupertinoNavigationBarData,
              trailingActions: <Widget>[
                PlatformIconButton(
                  padding: EdgeInsets.zero,
                  iosIcon: Icon(CupertinoIcons.share),
                  androidIcon: Icon(Icons.share),

                  ios: (_) => myCupertinoIconButtonData,
                  android: (_) => myMaterialIconButtonData,
                  onPressed: () {},
                ),
              ],
            ),
            //body: ,
          ),
        ),
      ),
    );
  }
}
