# Templates

I use an approach to build variants that gitignores the build variant files and the dependency injecting which results in no api keys in the git repo.
This directory contains templates that show what is set in each build variant and how the dependency injection is setup.